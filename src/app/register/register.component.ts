import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';

import {AuthService} from '../auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  error: string = null;
  success: boolean = false;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }

    const email = form.value.email;
    const password = form.value.password;

    this.authService.register(email, password).subscribe(
      resData => {
        this.error = null;
        this.success = true;
        setTimeout(() => {
            this.router.navigate(['/login']);
          },
          20000
        );
      },
      errorMessage => {
        console.log('register error');
        this.error = errorMessage;
      }
    );
  }
}
