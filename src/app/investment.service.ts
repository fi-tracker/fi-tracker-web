import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Investment } from './model/investment.model';
import { environment } from '../environments/environment';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { InvestmentMetric } from './model/investmentMetric.model';

@Injectable({
  providedIn: 'root'
})
export class InvestmentService {

  private investmentsLoaded = false;
  private investments: Investment[] = [];
  private accountTypes: string[] = [];

  constructor(private http: HttpClient) { }

  loadAccountTypes() {
    return this.http.get<string[]>(environment.baseUrl + '/investment/account-types').pipe(
      tap(types => {
        this.accountTypes = types;
      })
    );
  }

  getAccountTypes() {
    return this.accountTypes;
  }

  loadInvestments() {
    if (this.investmentsLoaded) {
      return new Observable((observer) => {
        observer.next(this.investments);
      });
    } else {
      return this.http.get<Investment[]>(environment.baseUrl + '/investment').pipe(
        tap(investments => {
          this.setInvestments(investments);
        })
      );
    }
  }

  updateInvestment(investment: Investment) {
    return this.http.post<Investment>(environment.baseUrl + '/investment', investment).pipe(
      tap(investment => {
        this.addOrUpdateInvestment(investment);
      }
      )
    );
  }

  deleteInvestment(investment: Investment) {
    return this.http.request('delete', environment.baseUrl + '/investment/' + investment.id).pipe(
      tap(() => {
        this.removeInvestment(investment);
      })
    );
  }

  getInvestments() {
    return this.investments;
  }

  getInvestment(id: string) {
    for (let investment of this.investments) {
      if (investment.id === id) {
        return investment;
      }
    }
    return this.getDefaultInvestment();
  }

  reloadInvestment() {
    this.investmentsLoaded = false;
    return this.loadInvestments();
  }

  getInvestmentMetrics(id: string): Observable<InvestmentMetric[]> {
    return this.http.get<InvestmentMetric[]>(`${environment.baseUrl}/investment/${id}/metrics`);
  }

  private getDefaultInvestment() {
    let investment = new Investment();
    investment.description = '';
    investment.type = '';
    investment.accountType = '';
    investment.costBasis = 0;
    investment.currentValue = 0;
    investment.currentDebt = 0;
    return investment;
  }

  private setInvestments(investments: Investment[]) {
    this.investments = investments;
    this.investmentsLoaded = true;
  }

  private addOrUpdateInvestment(investment: Investment) {
    let newInvestment = true;
    this.investments.forEach((inv, index) => {
      if (inv.id === investment.id) {
        this.investments[index] = investment;
        newInvestment = false;
      }
    });
    if (newInvestment) {
      this.investments.push(investment);
    }
  }

  private removeInvestment(investment: Investment) {
    this.investments.forEach((inv, index) => {
      if (inv.id === investment.id) {
        this.investments.splice(index, 1);
      }
    });
  }
}
