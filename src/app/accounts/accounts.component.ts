import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AccountService } from '../account.service';
import { MessageService } from '../message.service';
import { Account } from '../model/account.model';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {

  mode = '';
  selectedAccount: Account;
  showDeleteModal = false;

  constructor(public accountService: AccountService, private messageService: MessageService) { }

  ngOnInit() {
    this.accountService.loadAcounts().subscribe(() => { },
      error => {
        if (!environment.production) {
          console.log(error);
        }
        this.messageService.setError("could not load accounts");
      });
  }

  newAccount() {
    this.selectedAccount = new Account();
    this.mode = 'new';
  }

  isAccountEditable(account: Account) {
    return this.mode === 'edit' && this.selectedAccount && this.selectedAccount.id === account.id;
  }

  editAccount(account: Account) {
    this.mode = 'edit';
    this.selectedAccount = account;
  }

  cancelEdit() {
    this.clearSelected();
  }

  deleteAccount(account: Account) {
    this.mode = 'delete';
    this.selectedAccount = account;
    this.showDeleteModal = true;
  }

  onCloseDeleteModal() {
    this.showDeleteModal = false;
  }

  onDelete() {
    this.accountService.deleteAccount(this.selectedAccount).subscribe(() => {
      this.clearSelected();
      this.showDeleteModal = false;
    });
  }

  saveAccount() {
    if (this.validateAccount()) {
      this.selectedAccount.id ?
        this.accountService.updateAccount(this.selectedAccount).subscribe(() => {
          this.clearSelected();
        }) :
        this.accountService.createAccount(this.selectedAccount).subscribe(() => {
          this.clearSelected();
        });
    }
  }

  validateAccount() {
    return this.selectedAccount && this.selectedAccount.description;
  }

  private clearSelected() {
    this.mode = '';
    this.selectedAccount = undefined;
  }
}
