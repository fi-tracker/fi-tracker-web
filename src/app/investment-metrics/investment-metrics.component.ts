import { Component, Input, OnInit } from '@angular/core';
import { InvestmentService } from '../investment.service';
import { InvestmentMetric } from '../model/investmentMetric.model';

@Component({
  selector: 'app-investment-metrics',
  templateUrl: './investment-metrics.component.html',
  styleUrls: ['./investment-metrics.component.css']
})
export class InvestmentMetricsComponent implements OnInit {

  @Input() investmentId: string;

  metrics: InvestmentMetric[] = [];

  constructor(private investmentService: InvestmentService) { }

  ngOnInit() {
    this.investmentService.getInvestmentMetrics(this.investmentId).subscribe((response: InvestmentMetric[]) => {
      this.metrics = response;
    });
  }
}
