import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestmentMetricsComponent } from './investment-metrics.component';

describe('InvestmentMetricsComponent', () => {
  let component: InvestmentMetricsComponent;
  let fixture: ComponentFixture<InvestmentMetricsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestmentMetricsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestmentMetricsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
