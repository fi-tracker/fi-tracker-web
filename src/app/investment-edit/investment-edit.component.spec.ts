import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestmentEditComponent } from './investment-edit.component';

describe('InvestmentEditComponent', () => {
  let component: InvestmentEditComponent;
  let fixture: ComponentFixture<InvestmentEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestmentEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestmentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
