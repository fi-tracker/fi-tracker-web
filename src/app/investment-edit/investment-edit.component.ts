import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InvestmentService } from '../investment.service';
import { Investment } from '../model/investment.model';
import { MessageService } from '../message.service';

import { ActivatedRoute, Params, Router } from '@angular/router';
import { isNullOrUndefined } from 'util';
import { environment } from '../../environments/environment';
import { TransactionService } from '../transaction.service';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-investment-edit',
  templateUrl: './investment-edit.component.html',
  styleUrls: ['./investment-edit.component.css']
})
export class InvestmentEditComponent implements OnInit {

  investmentForm: FormGroup;
  investment: Investment;
  loading = true;
  editMode = false;
  showDeleteModal = false;

  constructor(private investmentService: InvestmentService,
    private transactionService: TransactionService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private router: Router,
    public accountService: AccountService) { }

  ngOnInit() {
    this.transactionService.loadTransactionTypes();
    this.route.params.subscribe((params: Params) => {
      this.initForm(params['id']);
    });
    this.accountService.loadAcounts().subscribe(() => { },
      error => {
        if (!environment.production) {
          console.log(error);
        }
        this.messageService.setError("could not load accounts");
      });
  }

  onSubmit() {
    if (!environment.production) {
      console.log(this.investmentForm);
    }
    if (this.investmentForm.valid) {
      this.loading = true;
      this.investmentService.updateInvestment(this.investmentForm.value).subscribe((response: Investment) => {
        if (!environment.production) {
          console.log(response);
        }
        this.messageService.setSuccessWithAutoClear('investment saved successfully');
        if (this.editMode) {
          this.loading = false;
        } else {
          this.router.navigate(['/investment/' + response.id]);
        }
      });
    } else {
      this.messageService.setError('invalid form data, please correct and submit again');
    }
  }

  onDelete() {
    this.showDeleteModal = false;
    this.loading = true;
    this.investmentService.deleteInvestment(this.investment).subscribe(() => {
      this.messageService.setSuccessWithAutoClear('investment deleted successfully');
      this.router.navigate(['/investments']);
    });
  }

  onCloseForm() {
    this.messageService.clearMessages();
  }

  onConfirmDelete() {
    this.showDeleteModal = true;
  }

  onCloseDeleteModal() {
    this.showDeleteModal = false;
  }

  onTransactionModified() {
    console.log('transaction modified');
    this.reloadInvestment();
  }

  reloadInvestment() {
    this.investmentService.reloadInvestment().subscribe(() => {
      this.investment = this.investmentService.getInvestment(this.investment.id);
      this.investmentForm.controls.currentValue.setValue(this.investment.currentValue);
      this.investmentForm.controls.costBasis.setValue(this.investment.costBasis);
      this.investmentForm.controls.currentDebt.setValue(this.investment.currentDebt);
    });
  }

  private initForm(id: string) {

    this.investmentService.loadInvestments().subscribe(() => {

      this.investment = this.investmentService.getInvestment(id);

      this.investmentForm = new FormGroup({
        'description': new FormControl(this.investment.description, [Validators.required, Validators.minLength(4)]),
        'type': new FormControl(this.investment.type, Validators.required),
        'accountType': new FormControl(this.investment.accountType, Validators.required),
        'costBasis': new FormControl(this.investment.costBasis, Validators.required),
        'currentDebt': new FormControl(this.investment.currentDebt, Validators.required),
        'currentValue': new FormControl(this.investment.currentValue, Validators.required),
        'accountId': new FormControl(this.investment.accountId),
        'id': new FormControl(this.investment.id)
      });

      if (!isNullOrUndefined(this.investment.id)) {
        this.editMode = true;

        this.transactionService.loadTransactions(this.investment.id).subscribe(() => {
          this.loading = false;
        });

      } else {
        this.transactionService.clearTransactions();
        this.loading = false;
      }

    })
  }
}
