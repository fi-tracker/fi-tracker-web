import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { AuthService } from '../auth/auth.service';
import { User } from '../model/user.model';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  loading = true;
  userForm: FormGroup;
  error: string = null;
  message: string = null;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.info().subscribe((user: User) => {
      this.userForm = new FormGroup({
        email: new FormControl(this.authService.user.getValue().email),
        password: new FormControl(),
        password_confirm: new FormControl(),
        monthlyExpenses: new FormControl(user.monthlyExpenses),
        targetIncome: new FormControl(user.targetIncome),
        retirementWithdrawRate: new FormControl(user.retirementWithdrawRate)
      });
      this.loading = false;
    });
  }

  onSubmit() {
    this.loading = true;
    this.message = null;
    if (this.userForm.value.password != null && this.userForm.value.password !== this.userForm.value.password_confirm) {
      this.error = 'passwords do not match';
      this.loading = false;
    } else {
      this.error = null;
      this.authService.save(this.userForm.value).subscribe(
        resData => {
          this.error = null;
          this.loading = false;
          this.message = 'account updated successfully';
        },
        errorMessage => {
          this.error = errorMessage;
          this.loading = false;
        }
      );
    }
  }
}
