import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ChartsModule, ThemeService } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { AccountComponent } from './account/account.component';
import { AuthInterceptorService } from './auth/auth-interceptor.service';
import { InvestmentsComponent } from './investments/investments.component';
import { ReportsComponent } from './reports/reports.component';
import { InvestmentEditComponent } from './investment-edit/investment-edit.component';
import { AlertComponent } from './alert/alert.component';
import { LoadingComponent } from './loading/loading.component';
import { TransactionComponent } from './transaction/transaction.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { InvestmentMetricsComponent } from './investment-metrics/investment-metrics.component';
import { AccountsComponent } from './accounts/accounts.component';
import { FreedomTrackerComponent } from './freedom-tracker/freedom-tracker.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    AccountComponent,
    InvestmentsComponent,
    ReportsComponent,
    InvestmentEditComponent,
    AlertComponent,
    LoadingComponent,
    TransactionComponent,
    PortfolioComponent,
    InvestmentMetricsComponent,
    AccountsComponent,
    FreedomTrackerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ChartsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
    ThemeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
