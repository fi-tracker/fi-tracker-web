export class MonthlySummary {
  public year: number;
  public month: number;
  public amount: number;
}
