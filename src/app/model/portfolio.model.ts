import { MonthlySummary } from './monthlySummary.model';
import InvestmentSummary from './investmentSummary.model';

export class Portfolio {
  public investments: number;
  public portfolioValue: number;
  public portfolioDebt: number;
  public portfolioEquity: number;
  public cashReserves: number;
  public monthsReserve: number;
  public targetIncome: number;
  public accountTypeBalances: object;
  public investmentTypeBalances: object;
  public passiveIncome: MonthlySummary[];
  public investmentSummaries: InvestmentSummary[];
}
