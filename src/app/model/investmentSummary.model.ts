import { Investment } from './investment.model';

export default class InvestmentSummary {    
    public investment: Investment;
    public income: number;
    public appreciation: number;
    public roi: number;
    public capRate: number;
}