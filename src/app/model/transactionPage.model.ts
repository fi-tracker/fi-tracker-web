import { Transaction } from "./transaction.model";

export class TransactionPage {
    public content: Transaction[];
    public totalPages: number;
    public size: number;
}