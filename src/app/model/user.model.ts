export class User {

  public email: string;
  public password: string;
  public monthlyExpenses: number;
  public targetIncome: number;
  public retirementWithdrawRate: number;
  public token: string;
  public tokenExpiration: Date;

  constructor(email: string, token: string, tokenExpiration: Date) {
    this.email = email;
    this.token = token;
    this.tokenExpiration = tokenExpiration;
  }
}
