export class InvestmentMetric {
    public description: string;
    public monthlyAverage: number;
    public yearlyTotal: number;
    public lifetimeTotal: number;
    public metricType: string;
}