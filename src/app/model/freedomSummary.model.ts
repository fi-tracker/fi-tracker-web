import { Investment } from "./investment.model";

export class FreedomSummary {
    public monthlyExpenses: number;
    public preRetirementInvestments: PreRetirementInvestment[];
    public postRetirementInvestments: PostRetirementInvestment[];
}

export class PreRetirementInvestment {
    public investment: Investment;
    public annualCashReturn: number;
    public averageMonthlyCashflow: number;
}

export class PostRetirementInvestment {
    public investment: Investment;
    public annualWithdrawRate: number;
    public monthlyWithdrawAmount: number;
}