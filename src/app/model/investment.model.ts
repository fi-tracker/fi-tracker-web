export class Investment {
  public id: string;
  public description: string;
  public type: string;
  public costBasis: number;
  public currentDebt: number;
  public currentValue: number;
  public accountType: string;
  public accountId?: string;
}
