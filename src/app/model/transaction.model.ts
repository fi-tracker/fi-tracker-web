export class Transaction {
  public id: string;
  public investmentId: string;
  public transactionDate: string;
  public type: string;
  public description: string;
  public amount: number;

  constructor(){
    this.id = "";
    this.investmentId = "";
    let currentDate = new Date();
    this.transactionDate = currentDate.getFullYear() + "-" + (currentDate.getMonth() + 1).toString().padStart(2, "0") + "-" + currentDate.getDate().toString().padStart(2, "0");
    this.type = "";
    this.description = "";
    this.amount = 0;
  }
}
