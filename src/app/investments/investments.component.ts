import { Component, OnInit } from '@angular/core';
import { InvestmentService } from '../investment.service';
import { MessageService } from '../message.service';
import { environment } from '../../environments/environment';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-investments',
  templateUrl: './investments.component.html',
  styleUrls: ['./investments.component.css']
})
export class InvestmentsComponent implements OnInit {

  investmentLoaded = false;
  accountsLoaded = false;

  constructor(public investmentService: InvestmentService, private messageService: MessageService, public accountService: AccountService) { }

  ngOnInit() {
    this.investmentService.loadInvestments().subscribe(
      () => {
        this.investmentLoaded = true;
      }, error => {
        if (!environment.production) {
          console.log(error);
        }
        this.investmentLoaded = true;
        this.messageService.setError("could not load investments");
      });
    this.accountService.loadAcounts().subscribe(() => {
      this.accountsLoaded = true;
    },
      error => {
        if (!environment.production) {
          console.log(error);
        }
        this.accountsLoaded = true;
        this.messageService.setError("could not load accounts");
      });
  }

  getInvestmentsWithoutAccounts() {
    return this.investmentService.getInvestments() && this.investmentService.getInvestments().filter(investment => !investment.accountId);
  }

  getAccountsWithInvestments() {
    return this.accountService.getAccounts() && this.investmentService.getInvestments() &&
      this.accountService.getAccounts().filter(account =>
        this.investmentService.getInvestments().filter(investment => investment.accountId === account.id).length > 0);
  }

  getInvestmentsForAccountId(accountId) {
    return this.investmentService.getInvestments() &&
      this.investmentService.getInvestments().filter(investment => investment.accountId === accountId);
  }

  getCurrentValueForAccountId(accountId) {
    const investments = this.getInvestmentsForAccountId(accountId);
    return investments ? investments.map(investment => investment.currentValue).reduce((prev, curr) => prev + curr, 0) : 0;
  }
}
