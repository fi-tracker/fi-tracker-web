import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Transaction } from './model/transaction.model';

import { environment } from '../environments/environment';
import { tap } from 'rxjs/operators';
import { TransactionPage } from './model/transactionPage.model';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  private transactions: Transaction[] = [];
  private transactionTypes: string[] = [];
  private pages: number = 0;
  private pageSize: number = 20;
  private pageSizeIncrement: number = 20;
  private lastInvestmentId: string = "";

  constructor(private http: HttpClient) { }

  loadTransactionTypes() {
    if (this.transactionTypes.length === 0) {
      this.http.get<string[]>(environment.baseUrl + '/transaction/types').subscribe(types => {
        this.transactionTypes = types;
      });
    }
  }

  loadTransactions(investmentId: string) {
    if (this.lastInvestmentId !== investmentId) {
      this.lastInvestmentId = investmentId;
      this.pageSize = this.pageSizeIncrement;
    }
    return this.http.get<TransactionPage>(environment.baseUrl + '/transaction/' + investmentId + '?pageSize=' + this.pageSize).pipe(
      tap(page => {
        this.setTransactions(page.content);
        this.pages = page.totalPages;
      })
    );
  }

  reloadTransactions() {
    this.loadTransactions(this.lastInvestmentId).subscribe(() => { });
  }

  saveTransaction(transaction: Transaction) {
    return this.http.post<Transaction>(environment.baseUrl + '/transaction', transaction).pipe(
      tap(() => {
        this.reloadTransactions();
      }
      ));
  }

  deleteTransaction(transaction: Transaction) {
    return this.http.delete(environment.baseUrl + '/transaction/' + transaction.id).pipe(
      tap(() => {
        this.reloadTransactions();
      })
    );
  }

  setTransactions(transactions: Transaction[]) {
    this.transactions = transactions;
  }

  getTransactions() {
    return this.transactions;
  }

  clearTransactions() {
    this.transactions = [];
  }

  getTransactionTypes() {
    return this.transactionTypes;
  }

  hasMorePages() {
    return this.pages > 1;
  }

  loadMorePages() {
    this.pageSize += this.pageSizeIncrement;
    this.reloadTransactions();
  }
}
