import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private success = '';
  private error = '';
  private info = '';
  private timer = null;

  constructor() { }

  getSuccess() {
    return this.success;
  }

  getError() {
    return this.error;
  }

  getInfo() {
    return this.info;
  }

  setSuccess(message: string) {
    this.clearMessages();
    this.success = message;
  }

  setSuccessWithAutoClear(message: string) {
    this.setSuccess(message);
    this.delayedClearMessage();
  }

  setError(message: string) {
    this.clearMessages();
    this.error = message;
  }

  setErrorWithAutoClear(message: string) {
    this.setError(message);
    this.delayedClearMessage();
  }

  setInfo(message: string) {
    this.clearMessages();
    this.info = message;
  }

  setInfoWithAutoClear(message: string) {
    this.setInfo(message);
    this.delayedClearMessage();
  }

  clearMessages() {
    this.success = '';
    this.error = '';
    this.info = '';
    if (this.timer != null) {
      clearInterval(this.timer);
    }
  }

  delayedClearMessage() {
    this.timer = setInterval(() => {
      console.log('clearing message');
      this.clearMessages();
    }, 3000);
  }
}
