import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { catchError, tap } from 'rxjs/operators';
import { throwError, BehaviorSubject } from 'rxjs';

import { AuthResponse } from './auth.response';
import {User} from '../model/user.model';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user = new BehaviorSubject<User>(null);
  private tokenExpirationTimer: any;

  constructor(private http: HttpClient, private router: Router) { }

  login(email: string, password: string) {

    const formData = new FormData();
    formData.append('username', email);
    formData.append('password', password);

    return this.http.post<AuthResponse>(
      environment.baseUrl + '/user/auth',
      formData
    ).pipe(
      catchError(this.handleError),
      tap(resData => {
        this.handleAuthentication(
          email,
          resData.token,
          resData.expiration
        );
      })
    );
  }

  info() {
    return this.http.get<User>(environment.baseUrl + '/user/info');
  }

  register(email: string, password: string) {

    return this.http.post<AuthResponse>(
      environment.baseUrl + '/user/register',
      {
        email: email,
        password: password
      }
    ).pipe(
      catchError(this.handleError)
    );
  }

  save(user: User) {
    return this.http.post<AuthResponse>(
      environment.baseUrl + '/user/save',
      user
    ).pipe(
      catchError(this.handleError),
      tap(resData => {
        this.handleAuthentication(
          user.email,
          resData.token,
          resData.expiration
        );
      })
    );
  }

  autoLogin() {

    const userData: {
      email: string;
      token: string;
      tokenExpiration: string;
    } = JSON.parse(localStorage.getItem('userData'));
    if (!userData) {
      return;
    }

    const loadedUser = new User(
      userData.email,
      userData.token,
      new Date(userData.tokenExpiration)
    );

    if (loadedUser.token) {
      this.user.next(loadedUser);
      const expirationDuration =
        new Date(userData.tokenExpiration).getTime() -
        new Date().getTime();
      this.autoLogout(expirationDuration);
    }
  }

  logout() {
    this.user.next(null);
    this.router.navigate(['/login']);
    localStorage.removeItem('userData');
    if (this.tokenExpirationTimer) {
      clearTimeout(this.tokenExpirationTimer);
    }
    this.tokenExpirationTimer = null;
  }

  autoLogout(expirationDuration: number) {
    this.tokenExpirationTimer = setTimeout(() => {
      this.logout();
    }, expirationDuration);
  }

  private handleAuthentication(
    email: string,
    token: string,
    expiresIn: number
  ) {
    const expirationDate = new Date(expiresIn);
    const user = new User(email, token, expirationDate);
    this.user.next(user);
    const millisecondsToExpire = expiresIn - new Date().getTime();
    this.autoLogout(millisecondsToExpire);
    localStorage.setItem('userData', JSON.stringify(user));
  }

  private handleError(errorRes: HttpErrorResponse) {

    console.log(errorRes);

    if (errorRes.error && errorRes.statusText && errorRes.statusText === 'OK') {
      if (errorRes.error.error) {
        return throwError(errorRes.error.error);
      }
      return throwError(errorRes.error);
    }
    return throwError('invalid username / password');
  }
}
