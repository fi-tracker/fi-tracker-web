export class AuthResponse {
  token: string;
  expiration: number;

  public getToken() {
    return this.token;
  }
}
