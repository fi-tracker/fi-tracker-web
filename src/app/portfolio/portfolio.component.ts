import { Component, OnInit } from '@angular/core';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

import { PortfolioService } from '../portfolio.service';
import { Portfolio } from '../model/portfolio.model';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {
  loaded = false;
  currencyPipe = new CurrencyPipe('en');
  includeRetirement = true;

  // PIE CHART SETTINGS
  public pieChartOptions: ChartOptions = {
    responsive: true,
    aspectRatio: 3,
    legend: {
      position: 'right'
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        }
      }
    },
    tooltips: {
      enabled: true,
      mode: 'single',
      callbacks: {
        label: (tooltipItem, data) => {
          const label = data.labels[tooltipItem.index];
          const datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          const formattedNumber = this.currencyPipe.transform(
            datasetLabel,
            'USD',
            'symbol'
          );
          return label + ': ' + formattedNumber;
        }
      }
    }
  };
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public accountTypeChartLabels: Label[] = [];
  public accountTypeChartData: any[] = [];
  public investmentTypeChartLabels: Label[] = [];
  public investmentTypeChartData: any[] = [];

  // BAR CHART SETTINGS
  public barChartOptions: ChartOptions = {
    responsive: true,
    aspectRatio: 4,
    scales: {
      xAxes: [{}],
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            callback: (value, index, values) => {
              return this.currencyPipe.transform(value, 'USD', 'symbol');
            }
          }
        }
      ]
    },
    tooltips: {
      callbacks: {
        label: (tooltipItem, data) => {
          return this.currencyPipe.transform(tooltipItem.yLabel, 'USD', 'symbol');
        }
      }
    },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end'
      }
    }
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartData: ChartDataSets[] = [];

  constructor(public portfolioService: PortfolioService) { }

  ngOnInit() {
    this.loadPortfolio();
  }

  onToggleIncludeRetirement() {
    this.includeRetirement = !this.includeRetirement;
    this.loadPortfolio();
  }

  private convertMonthNumberToName(number: number): string {
    const months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];
    return months[number - 1] || '';
  }

  private loadPortfolio() {
    this.portfolioService.loadPortfolio(this.includeRetirement).subscribe((portfolio: Portfolio) => {

      // load balance by account type chart
      this.accountTypeChartLabels = [];
      this.accountTypeChartData = [];
      if (portfolio.accountTypeBalances) {
        for (const accountType in portfolio.accountTypeBalances) {
          if (portfolio.accountTypeBalances.hasOwnProperty(accountType)) {
            this.accountTypeChartLabels.push(accountType);
            this.accountTypeChartData.push(
              portfolio.accountTypeBalances[accountType]
            );
          }
        }
      }

      // load balance by investment type chart
      this.investmentTypeChartLabels = [];
      this.investmentTypeChartData = [];
      if (portfolio.investmentTypeBalances) {
        for (const investmentType in portfolio.investmentTypeBalances) {
          if (portfolio.investmentTypeBalances.hasOwnProperty(investmentType)) {
            this.investmentTypeChartLabels.push(investmentType);
            this.investmentTypeChartData.push(
              portfolio.investmentTypeBalances[investmentType]
            );
          }
        }
      }

      // load passive income
      this.barChartLabels = [];
      this.barChartData = [];
      if (portfolio.passiveIncome) {
        const summaryData: number[] = [];
        const monthlyGoal: number[] = [];
        portfolio.passiveIncome.forEach(summary => {
          this.barChartLabels.push((this.convertMonthNumberToName(summary.month) + ' ' + summary.year) as Label);
          summaryData.push(summary.amount);
          monthlyGoal.push(portfolio.targetIncome);
        });
        this.barChartData.push({ data: summaryData, label: 'Passive Income' });
        this.barChartData.push({ data: monthlyGoal, label: 'Monthly Goal', type: 'line', fill: false });
      }

      this.loaded = true;
    });
  }
}
