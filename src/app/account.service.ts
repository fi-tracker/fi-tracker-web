import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Account } from './model/account.model';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private acccountsLoaded = false;
  private accounts: Account[] = [];

  constructor(private http: HttpClient) { }

  loadAcounts() {
    if (this.acccountsLoaded) {
      return new Observable((observer) => {
        observer.next(this.accounts);
      });
    } else {
      return this.http.get<Account[]>(environment.baseUrl + '/accounts').pipe(
        tap(accounts => {
          this.setAccounts(accounts);
        })
      );
    }
  }

  isAccountsLoaded() {
    return this.acccountsLoaded;
  }

  getAccounts() {
    return this.accounts;
  }

  createAccount(account: Account) {
    return this.http.post<Account[]>(environment.baseUrl + '/accounts', account).pipe(
      tap(accounts => {
        this.setAccounts(accounts);
      }
      )
    );
  }

  updateAccount(account: Account) {
    return this.http.put<Account[]>(`${environment.baseUrl}/accounts/${account.id}`, account).pipe(
      tap(accounts => {
        this.setAccounts(accounts);
      }
      )
    );
  }

  deleteAccount(account: Account) {
    return this.http.request<Account[]>('delete', `${environment.baseUrl}/accounts/${account.id}`).pipe(
      tap(accounts => {
        this.setAccounts(accounts);
      })
    );
  }

  private setAccounts(accounts: Account[]) {
    this.accounts = accounts;
    this.acccountsLoaded = true;
  }
}
