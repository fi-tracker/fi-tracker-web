import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Portfolio } from './model/portfolio.model';
import { FreedomSummary } from './model/freedomSummary.model';

@Injectable({
  providedIn: 'root'
})
export class PortfolioService {

  public portfolio: Portfolio;
  public freedomSummary: FreedomSummary;

  constructor(private http: HttpClient) { }

  loadPortfolio(includeRetirement: boolean) {
    return this.http.get<Portfolio>(`${environment.baseUrl}/portfolio?includeRetirement=${includeRetirement}`).pipe(
      tap(portfolio => {
        this.portfolio = portfolio;
      })
    );
  }

  loadFreedomTracker() {
    return this.http.get<FreedomSummary>(`${environment.baseUrl}/portfolio/freedom-tracker`).pipe(
      tap(freedomSummary => {
        this.freedomSummary = freedomSummary;
      })
    );
  }
}
