import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {TransactionService} from '../transaction.service';
import {Transaction} from '../model/transaction.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {

  @Input() investmentId: string;
  @Output() transactionModified = new EventEmitter<void>();

  editMode = false;
  transactionSubmitted = false;
  showDeleteModal = false;
  selectedTransaction: Transaction = new Transaction();
  transactionForm: FormGroup;

  constructor(public transactionService: TransactionService) { }

  ngOnInit() {

  }

  newTransaction() {
    this.editTransaction(new Transaction());
  }

  editTransaction(transaction: Transaction) {
    this.selectedTransaction = transaction;
    this.transactionForm = new FormGroup({
      'transactionDate': new FormControl(transaction.transactionDate, Validators.required),
      'type': new FormControl(transaction.type, Validators.required),
      'description': new FormControl(transaction.description, Validators.required),
      'amount': new FormControl(transaction.amount, Validators.required)
    });
    this.transactionSubmitted = false;
    this.editMode = true;
  }

  cancelTransaction() {
    this.editMode = false;
    this.selectedTransaction = new Transaction();
  }

  saveTransaction() {

    if(this.validateTransaction(this.selectedTransaction)) {
      this.selectedTransaction.investmentId = this.investmentId;
      this.transactionService.saveTransaction(this.selectedTransaction).subscribe(() => {
        this.editMode = false;
        this.selectedTransaction = new Transaction();
        this.transactionModified.emit();
      });
    }

    this.transactionSubmitted = true;
  }

  deleteTransaction(transaction: Transaction) {
    this.selectedTransaction =  transaction;
    this.showDeleteModal = true;
  }

  onCloseDeleteModal() {
    this.showDeleteModal = false;
  }

  onDelete() {
    this.transactionService.deleteTransaction(this.selectedTransaction).subscribe(() => {
      this.showDeleteModal = false;
      this.transactionModified.emit();
    });
  }

  validateTransaction(transaction: Transaction) {

    this.transactionForm = new FormGroup({
      'transactionDate': new FormControl(transaction.transactionDate, Validators.required),
      'type': new FormControl(transaction.type, Validators.required),
      'description': new FormControl(transaction.description, Validators.required),
      'amount': new FormControl(transaction.amount, Validators.required)
    });

    if (!this.transactionForm.valid) {
      return false;
    }

    return true;
  }
}
