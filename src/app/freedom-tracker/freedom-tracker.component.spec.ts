import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreedomTrackerComponent } from './freedom-tracker.component';

describe('FreedomTrackerComponent', () => {
  let component: FreedomTrackerComponent;
  let fixture: ComponentFixture<FreedomTrackerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreedomTrackerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreedomTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
