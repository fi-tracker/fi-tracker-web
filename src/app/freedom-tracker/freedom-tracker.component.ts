import { Component, OnInit } from '@angular/core';
import { FreedomSummary } from '../model/freedomSummary.model';
import { PortfolioService } from '../portfolio.service';

@Component({
  selector: 'app-freedom-tracker',
  templateUrl: './freedom-tracker.component.html',
  styleUrls: ['./freedom-tracker.component.css']
})
export class FreedomTrackerComponent implements OnInit {

  loaded = false;

  constructor(public portfolioService: PortfolioService) { }

  ngOnInit() {
    this.portfolioService.loadFreedomTracker().subscribe((freedomSummary: FreedomSummary) => {
      this.loaded = true;
    });
  }

  getPreRetirementCashflowTotal() {
    return this.portfolioService.freedomSummary.preRetirementInvestments
      .map(investment => investment.averageMonthlyCashflow)
      .reduce((prev, curr) => prev + curr);
  }

  getPreRetirementOverUnder() {
    return this.getPreRetirementCashflowTotal() - this.portfolioService.freedomSummary.monthlyExpenses;
  }

  getPostRetirementWithdrawTotal() {
    return this.portfolioService.freedomSummary.postRetirementInvestments
      .map(investment => investment.monthlyWithdrawAmount)
      .reduce((prev, curr) => prev + curr);
  }

  getPostRetirementOverUnder() {
    return this.getPostRetirementWithdrawTotal() - this.portfolioService.freedomSummary.monthlyExpenses;
  }

  getCombinedIncomeTotal() {
    return this.getPreRetirementCashflowTotal() + this.getPostRetirementWithdrawTotal();
  }

  getCombinedOverUnder() {
    return this.getCombinedIncomeTotal() - this.portfolioService.freedomSummary.monthlyExpenses;
  }
}
